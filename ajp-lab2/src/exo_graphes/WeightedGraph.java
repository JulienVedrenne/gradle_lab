package exo_graphes;


public interface WeightedGraph<V,E> extends Graph<V> {
	
	boolean addEdge(V fromVertex , V toVertex , E edge);

}
