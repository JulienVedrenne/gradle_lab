package exo_graphes;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		UndirectedListGraph<String> graph= new UndirectedListGraph<>();
		ArrayList<String> liste= new ArrayList<String>();
//		String s1="a";
//		String s2="b";
//		String s3="c";
//		String s4="d";
//		String s5="e";
//		String s6="f";
//		
//		graph.addVertex(s1);
//		graph.addVertex(s2);
//		graph.addVertex(s3);
//		graph.addVertex(s4);
//		graph.addVertex(s5);
//		graph.addVertex(s6);
//		graph.addEdge(s1, s2);
//		graph.addEdge(s2, s3);
//		graph.addEdge(s2, s5);
//		graph.addEdge(s3, s4);
//		graph.addEdge(s3, s1);
//		graph.addEdge(s3, s6);
//		graph.addEdge(s4, s2);
		
		String s1="a";
		String s2="b";
		String s3="c";
		
		graph.addVertex(s1);
		graph.addVertex(s2);
		graph.addVertex(s3);
		
		graph.addEdge(s1, s2);
		graph.addEdge(s1, s3);
		graph.addEdge(s2, s3);
		
		System.out.println(graph.getDotType());
		
//		DepthFirstSearch d = new DepthFirstSearch(graph,s1);
//		liste=(ArrayList<String>) d.execute(graph, s1);
//		System.out.println(liste.toString());
//		
//		String st = graph.toString();
//		System.out.println(st);
	}
}
