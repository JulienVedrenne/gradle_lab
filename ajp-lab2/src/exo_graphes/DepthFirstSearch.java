package exo_graphes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DepthFirstSearch {
	
	private boolean[] visit; 
	private List list;

	
	
	public <V> DepthFirstSearch(Graph<V> graph , V vertex) {
		HashMap<V, Set<V>> hm ;
		hm=graph.getHm();
		visit = new boolean[1000];
	
		list=new ArrayList<V>();

		
	}

	public <V> List<V> execute(Graph<V> graph , V vertex ){
	
		HashMap<V, Set<V>> hm ;
		
		visit[vertex.hashCode()]=true;
		list.add(vertex);
		Set<V> set1 = graph.getChildren(vertex);
		  for(V v : set1){

            	if(!visit[v.hashCode()]){
            	
            	execute(graph,v);
            	}
            }
		return list;
		
		
	}

}
